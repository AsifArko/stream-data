package connectiors

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-redis/redis"
	"gitlab.com/restra-core/gateway/lang"
	"gitlab.com/restra-core/gateway/models"
	"net/http"
	"time"
)

type RedisConnection struct {
	client *redis.Client
}

func Connect(cfg *models.Config, client *redis.Client) *RedisConnection {
	return &RedisConnection{
		client: client,
	}
}

func (redis *RedisConnection) SetValue(key string, value string, expiration time.Duration) error {
	err := redis.client.Set(key, value, expiration).Err()
	if err != nil {
		msg := fmt.Sprintf("RedisConnection :: Connect :: SetValue :: Error :: %s", err.Error())
		fmt.Println(msg)
		return errors.New(msg)
	}
	return nil
}

func (redis *RedisConnection) SetJSONValue(key string, value interface{}, expiration time.Duration) error {

	b, err := json.Marshal(value)
	if err != nil {
		return err
	}

	err = redis.client.Set(key, string(b), expiration).Err()
	if err != nil {
		msg := fmt.Sprintf("RedisConnection :: Connect :: SetValue :: Error :: %s", err.Error())
		fmt.Println(msg)
		return errors.New(msg)
	}
	return nil
}

func (redis *RedisConnection) GetJSONValue(key string) (interface{}, error) {
	data, err := redis.client.Get(key).Result()
	if err != nil {
		return nil, err
	}

	var jsonData interface{}
	json.Unmarshal([]byte(data), &jsonData)
	return jsonData, nil

}

func (r *RedisConnection) CheckRedisKey(prefix string, phone string) (*models.Message, error) {
	// Check whether the user exists or not ?
	msg, err := r.GetValue(fmt.Sprintf("%s:%s", prefix, phone), false)

	if err == redis.Nil {
		return &models.Message{
			Body: &models.Message{Code: http.StatusBadRequest, Message: lang.PHONE_NOT_FOUND},
		}, nil
	} else if err != nil {
		return nil, err
	}

	return &models.Message{
		Code: http.StatusOK,
		Body: msg,
	}, nil
}

// Get User Data from Redis
func (r *RedisConnection) getUserData(prefix string, phone string) (*models.User, error) {
	// Check whether the user exists or not ?
	user, err := r.GetUserJSONValue(fmt.Sprintf("%s:%s", prefix, phone))

	if err == redis.Nil {
		return nil, errors.New(fmt.Sprintf("Fuck off ! `%s` is not registered with us", phone))
	} else if err != nil {
		return nil, err
	}

	return user, nil
}

func (redis *RedisConnection) GetUserJSONValue(key string) (*models.User, error) {
	data, err := redis.client.Get(key).Result()
	if err != nil {
		return nil, err
	}

	var jsonData models.User
	json.Unmarshal([]byte(data), &jsonData)
	return &jsonData, nil

}

func (redis *RedisConnection) SetUserJSONValue(key string, value *models.User) (*models.User, error) {

	b, err := json.Marshal(value)
	if err != nil {
		return nil, err
	}

	err = redis.client.Set(key, string(b), 0).Err()
	if err != nil {
		return nil, err
	}

	return redis.GetUserJSONValue(key)
}

func (redis *RedisConnection) GetValue(key string, expire bool) (string, error) {
	if expire {
		// Expire after 1 minute
		redis.SetValue(key, "expiring..", time.Second)
		return redis.client.Get(key).Result()
	}
	return redis.client.Get(key).Result()
}

func (redis *RedisConnection) AddToSets(key string, value string) error {

	err := redis.client.SAdd(key, value).Err()
	if err != nil {
		msg := fmt.Sprintf("RedisConnection :: Connect :: Set Add :: Error :: %s", err.Error())
		fmt.Println(msg)
		return errors.New(msg)
	}
	return nil
}

func (redis *RedisConnection) RemoveSets(key string, value string) error {

	err := redis.client.SRem(key, value).Err()
	if err != nil {
		msg := fmt.Sprintf("RedisConnection :: Connect :: Set Add :: Error :: %s", err.Error())
		fmt.Println(msg)
		return errors.New(msg)
	}
	return nil
}
