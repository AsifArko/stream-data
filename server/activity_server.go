package server

import (
	"context"
	"fmt"
	"gitlab.com/post_activity/connections"
	"gitlab.com/post_activity/models"
	"gitlab.com/restra-core/buffers/common"
	"gitlab.com/restra-core/buffers/place"
	"gitlab.com/restra-core/buffers/profile"
	"strings"
)

type ActivityServer struct {
	//Configuration
	cfg *models.Config

	//Databases
	bolt      *connectiors.BoltConnection
	redis     *connectiors.RedisConnection
	couchbase *connectiors.DBConnection
}

func GetActivityServer(cbConn *connectiors.DBConnection, boltConn *connectiors.BoltConnection, redisConn *connectiors.RedisConnection, cfg *models.Config) (*ActivityServer, error) {
	return &ActivityServer{
		//config
		cfg: cfg,
		//databases
		bolt:      boltConn,
		couchbase: cbConn,
		redis:     redisConn,
	}, nil
}

// GET UserProfile && TravelPost from History and Action concurrently

func (server *ActivityServer) GetUserProfileFromPostHistory(ctx context.Context, in *common.Request) (chan *profile.ProfileInfo, error) {
	//Get the History document first
	history, err := server.GetHistory(ctx, in)
	if err != nil {
		panic(err)
	}

	//request := &common.Request{Id: history.Id, Type: history.Type}
	data := make(chan *profile.ProfileInfo)
	go func() {
		id := fmt.Sprintf("%s", history.History.Actor.Id)
		splitedSlice := strings.Split(id, "::")

		profile, err := server.GetUserProfile(ctx, &common.Request{Id: splitedSlice[1], Type: splitedSlice[1]})
		if err != nil {
			panic(err)
		}
		data <- profile
	}()
	fmt.Println(<-data)
	return data, nil
}

func (server *ActivityServer) GetTravelPostFromAction(ctx context.Context, in *common.Request) (chan *place.TravelPostParameter, error) {

	//Get the Action document first
	action, err := server.GetAction(ctx, in)
	if err != nil {
		panic(err)
	}

	data := make(chan *place.TravelPostParameter)
	go func() {
		id := fmt.Sprintf("%s", action.Action.Object)
		splitedSlice := strings.Split(id, "::")

		//Fetching the User post from couchbase
		post, err := server.GetTravelPost(ctx, &common.Request{Id: splitedSlice[1], Type: splitedSlice[0]})
		if err != nil {
			panic(err)
		}
		data <- post
	}()
	return data, nil
}

// All the GET & POST methods for 'ACTION' , 'HISTORY' , 'USER-PROFILE' , 'TRAVEL-POST'

func (server *ActivityServer) InsertAction(ctx context.Context, in *models.ActionRequest) (*models.ActionRequest, error) {
	//Generating the Action ID
	id := fmt.Sprintf("%s::%s", in.Type, in.Id)

	//Check if the action already exists
	var received models.ActionRequest
	_, err := server.couchbase.Get(id, &received)
	if received.Id == "" {
		//Insert the data
		_, err = server.couchbase.Insert(id, in, 0)
		if err != nil {
			return nil, errorMessage(err, "Error inserting data")
		}
	}

	return server.GetAction(ctx, &common.Request{Type: in.Type, Id: in.Id})
}

func (server *ActivityServer) GetAction(ctx context.Context, in *common.Request) (*models.ActionRequest, error) {
	//Generating the Action ID
	id := fmt.Sprintf("%s::%s", in.Type, in.Id)

	//data will be received in received
	var received models.ActionRequest
	_, err := server.couchbase.Get(id, &received)
	if err != nil {
		return nil, errorMessage(err, "Error in getting data")
	}
	return &received, nil
}

func (server *ActivityServer) InsertHistory(ctx context.Context, in *models.HistoryRequest) (*models.HistoryRequest, error) {
	//Generating the History ID
	id := fmt.Sprintf("%s::%s", in.Type, in.Id)

	// Check if exists
	var received models.HistoryRequest
	_, err := server.couchbase.Get(id, &received)
	if received.Id == "" {
		// Now Insert data and handle the error
		_, err = server.couchbase.Insert(id, &in, 0)
		if err != nil {
			return nil, errorMessage(err, "Error inserting data")
		}
	}
	return server.GetHistory(ctx, &common.Request{Type: in.Type, Id: in.Id})
}

func (server *ActivityServer) GetHistory(ctx context.Context, in *common.Request) (*models.HistoryRequest, error) {
	//Generating  a sigle post history data
	id := fmt.Sprintf("%s::%s", in.Type, in.Id)

	var history models.HistoryRequest
	_, err := server.couchbase.Get(id, &history)
	if history.Id == "" {
		if err != nil {
			errorMessage(err, "Error in getting document")
		}
	}
	return &history, nil
}

func (server *ActivityServer) InsertUserProfile(ctx context.Context, in *profile.ProfileInfo) (*profile.ProfileInfo, error) {
	//Generating the user id
	id := fmt.Sprintf("%s::%s", in.Type, in.Id)

	//Check if the data already exists
	var data profile.ProfileInfo
	_, err := server.couchbase.Get(id, &data)
	if data.Id == "" {
		//No data exists so insert
		_, err = server.couchbase.Insert(id, in, 0)
		if err != nil {
			errorMessage(err, "Data Insert Error")
			return nil, err
		}
	}
	return server.GetUserProfile(ctx, &common.Request{Id: in.Id, Type: in.Type})
}

func (server *ActivityServer) GetUserProfile(ctx context.Context, in *common.Request) (*profile.ProfileInfo, error) {
	//Generating the profile id
	id := fmt.Sprintf("%s::%s", in.Type, in.Id)

	//Travel Data from couchbase will be stored in data variable
	var data profile.ProfileInfo
	_, err := server.couchbase.Get(id, &data)
	if err != nil {
		errorMessage(err, "Error getting data")
		return nil, err
	}
	return &data, nil
}

func (server *ActivityServer) InsertTravelPost(ctx context.Context, in *place.TravelPostParameter) (*place.TravelPostParameter, error) {
	//Generating the Travel Post ID
	id := fmt.Sprintf("%s::%s", in.Body.Type, in.Body.Id)

	//Check if the data already exists
	var data place.TravelPostParameter
	_, err := server.couchbase.Get(id, &data)
	if data.Body.Id == "" {
		//No data exists so insert
		_, err = server.couchbase.Insert(id, in, 0)
		if err != nil {
			errorMessage(err, "Data Insert Error")
			return nil, err
		}
	}
	return server.GetTravelPost(ctx, &common.Request{Id: in.Body.Id, Type: in.Body.Type})
}

func (server *ActivityServer) GetTravelPost(ctx context.Context, in *common.Request) (*place.TravelPostParameter, error) {
	//Generating the post id
	id := fmt.Sprintf("%s::%s", in.Type, in.Id)

	//Travel Data from couchbase will be stored in data variable
	var data place.TravelPostParameter
	_, err := server.couchbase.Get(id, &data)
	if err != nil {
		errorMessage(err, "Error getting data")
		return nil, err
	}
	return &data, nil
}

func errorMessage(err error, context string) error {
	fmt.Println(context, err)
	return err
}
