package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/post_activity/connections"
	"gitlab.com/post_activity/models"
	"gitlab.com/post_activity/tests/helper"
	"gitlab.com/restra-core/buffers/common"
	"gitlab.com/restra-core/buffers/place"
	"gitlab.com/restra-core/buffers/profile"
)

var cfg *models.Config

func init() {
	cfg = &models.Config{
		//Couchbase information
		DBHost:         "localhost",
		DBPort:         "8091",
		NoSqlUser:      "Administrator",
		NoSqlPassword:  "nirfapurba",
		BucketName:     "socrates",
		BucketPassword: "",

		//Bolt information
		BoltHost:     "localhost",
		BoltPort:     "7687",
		BoltUser:     "neo4j",
		BoltPassword: "n4j",
	}
}

func main() {
	//conn, err := connectiors.GetBoltConnection(cfg)
	//if err != nil {
	//	errorMessage(err, "Error initiating couchbase")
	//}

	couchbase , _ := connectiors.GetDBConnection(cfg)
	/*
		//Get a single travel post

		//post , err := GetTravelPost(conn , &common.Request{Id: "1234", Type:"travel"})
		//if err != nil{
		//	errorMessage(err , "DB get")
		//}
		//fmt.Println(post)

		//Insert a single travel post

		//post := helper.GetTravelPost()
		//received , err := InsertTravelPost(conn , &post)
		//if err != nil{
		//	errorMessage(err , "DB insert")
		//}
		//fmt.Println(received)

		//prof , err := GetUserProfile(conn , &common.Request{Id: "4321", Type:"profile"})
		//if err != nil{
		//	errorMessage(err , "DB get")
		//}
		//fmt.Println(prof)

		//post := helper.GetUserProfile()
		//received , err := InsertUserProfile(conn , &post)
		//if err != nil{
		//	errorMessage(err , "DB insert")
		//}
		//fmt.Println(received)

		//GetTravelPostTagProfile(conn, &common.Request{Id: "1234", Type: "travel"})

		//action, err := InsertAction(conn, helper.GenerateSampleAction())
		//if err != nil {
		//	panic(err)
		//}

		//action,err := GetAction(conn , &common.Request{Id:"0001",Type:"action"})
		//if err != nil{
		//	panic(err)
		//}

		//history, err := InsertHistory(conn, helper.GenerateSampleHistory())
		//if err != nil {
		//	panic(err)
		//}
		//fmt.Printf("%+v\n", history)
	*/
	//GetUserProfileFromPostHistory(conn, &common.Request{Id: "0001", Type: "history"})
	//GetTravelPostFromAction(conn, &common.Request{Id: "0001", Type: "action"})

	//Insert a user profile
	prof := helper.GetUserProfile()
	x , _ := InsertUserProfile(couchbase, &prof)
	fmt.Println(x)

	//x , _ := FindUserFriends(conn, "1235") // Inside this function a go routine running concurrently to get the user friends
	////fmt.Println(<-x)
	//
	//for _ ,  i := range <-x{
	//	fmt.Println(i)
	//	buildUserProfileInCouchbaseFromNeo4j(i,couchbase)
	//}

}

func buildUserProfileInCouchbaseFromNeo4j(neoProfile []models.Node , conn *connectiors.DBConnection)  {
	var node models.Node
	node = neoProfile[0]

	var data profile.ProfileInfo

	data.Id = node.Properties.Id
	data.Name = &profile.Name{FirstName:node.Properties.Name}
	data.Address = &common.Address{
		District:&common.CodeSystem{
			Code:12,
			Display:node.Properties.HomeTown,
		},
	}
	data.Dob = node.Properties.Dob

	usprof , _ := InsertUserProfile(conn , &data)
	fmt.Println(usprof)

}

func FindUserFriends(bolt *connectiors.BoltConnection, uid string) (chan [][]models.Node, error) {

	//Make a channel to be returned in the parameter
	fChan := make (chan [][]models.Node)
	go func() {
		query := fmt.Sprintf("MATCH (n:Person)-[:FRIENDS]-(m:Person{id:\"%s\"}) return n", uid)
		data, _, _, err := bolt.Connection.QueryNeoAll(query, nil)
		if err != nil{
			errorMessage(err,"Error getting data from neo4j server")
		}

		// Serialize data
		b, err := json.Marshal(data)
		if err != nil {
			errorMessage(err,"Error Marshalling data into bytes")
		}

		//Deserialize data
		var friends [][]models.Node
		err = json.Unmarshal(b,&friends)
		if err != nil{
			errorMessage(err,"Error Marshalling data into bytes")
		}

		fChan <- friends
	}()

	//fmt.Println(<-fChan)

	return fChan , nil
}

func GetUserProfileFromPostHistory(conn *connectiors.DBConnection, in *common.Request) {
	history, err := GetHistory(conn, in)
	if err != nil {
		panic(err)
	}

	//request := &common.Request{Id: history.Id, Type: history.Type}
	data := make(chan profile.ProfileInfo)
	go func() {
		var profile profile.ProfileInfo
		id := fmt.Sprintf("%s", history.History.Actor.Id)
		_, err := conn.Get(id, &profile)
		if err != nil {
			panic(err)
		}
		data <- profile
	}()
	fmt.Println(<-data)
}

func GetTravelPostFromAction(conn *connectiors.DBConnection, in *common.Request) {
	action, err := GetAction(conn, in)
	if err != nil {
		panic(err)
	}

	//request := &common.Request{Id: history.Id, Type: history.Type}
	data := make(chan place.TravelPostParameter)
	go func() {
		var travelpost place.TravelPostParameter
		id := fmt.Sprintf("%s", action.Action.Object)
		_, err := conn.Get(id, &travelpost)
		if err != nil {
			panic(err)
		}
		data <- travelpost
	}()
	fmt.Println(<-data)
}

func InsertAction(conn *connectiors.DBConnection, in *models.ActionRequest) (*models.ActionRequest, error) {

	id := fmt.Sprintf("%s::%s", in.Type, in.Id)

	//Check if the action already exists
	var received models.ActionRequest
	_, err := conn.Get(id, &received)
	if received.Id == "" {
		//Insert the data
		_, err = conn.Insert(id, in, 0)
		if err != nil {
			return nil, errorMessage(err, "Error inserting data")
		}
	}

	data, err := GetAction(conn, &common.Request{Type: in.Type, Id: in.Id})
	if err != nil {
		return nil, errorMessage(err, "Error in getting data")
	}

	return data, nil
}

func GetAction(conn *connectiors.DBConnection, in *common.Request) (*models.ActionRequest, error) {
	id := fmt.Sprintf("%s::%s", in.Type, in.Id)

	var received models.ActionRequest
	_, err := conn.Get(id, &received)
	if err != nil {
		return nil, errorMessage(err, "Error in getting data")
	}
	return &received, nil
}

func InsertHistory(conn *connectiors.DBConnection, in *models.HistoryRequest) (*models.HistoryRequest, error) {
	id := fmt.Sprintf("%s::%s", in.Type, in.Id)

	// Check if exists
	var received models.HistoryRequest
	_, err := conn.Get(id, &received)
	if received.Id == "" {
		// Now Insert data and handle the error
		_, err = conn.Insert(id, &in, 0)
		if err != nil {
			return nil, errorMessage(err, "Error inserting data")
		}
	}
	data, err := GetHistory(conn, &common.Request{Type: in.Type, Id: in.Id})
	if err != nil {
		return nil, errorMessage(err, "Error in getting data")
	}
	return data, nil
}

func GetHistory(conn *connectiors.DBConnection, in *common.Request) (*models.HistoryRequest, error) {
	id := fmt.Sprintf("%s::%s", in.Type, in.Id)

	var history models.HistoryRequest
	_, err := conn.Get(id, &history)
	if history.Id == "" {
		fmt.Println("document not found")
		if err != nil {
			errorMessage(err, "Error in getting document")
		}
	}

	return &history, nil
}

//func InsertTravelPost(conn *connectiors.DBConnection, in *place.TravelPostParameter) (*place.TravelPostParameter, error) {
//	id := fmt.Sprintf("%s::%s", in.Body.Type, in.Body.Id)
//
//	//Check if the data already exists
//	var data place.TravelPostParameter
//	_, err := conn.Get(id, &data)
//	if data.Body == nil {
//		//No data exists so insert
//		_, err = conn.Insert(id, in, 0)
//		if err != nil {
//			errorMessage(err, "Data Insert Error")
//			return nil, err
//		}
//	}
//	return GetTravelPost(conn, &common.Request{Id: in.Body.Id, Type: in.Body.Type})
//}

//func GetTravelPost(conn *connectiors.DBConnection, in *common.Request) (*place.TravelPostParameter, error) {
//	//Generating the id
//	id := fmt.Sprintf("%s::%s", in.Type, in.Id)
//
//	//Travel Data from couchbase will be stored in data variable
//	var data place.TravelPostParameter
//	_, err := conn.Get(id, &data)
//	if err != nil {
//		errorMessage(err, "Error getting data")
//		return nil, err
//	}
//	return &data, nil
//}

func InsertUserProfile(conn *connectiors.DBConnection, in *profile.ProfileInfo) (*profile.ProfileInfo, error) {

	in.Type = "profile"

	id := fmt.Sprintf("%s::%s", in.Type, in.Id)

	//Check if the data already exists
	var data profile.ProfileInfo
	_, err := conn.Get(id, &data)
	if data.Id == "" {
		//No data exists so insert
		_, err = conn.Insert(id, in, 0)
		if err != nil {
			errorMessage(err, "Data Insert Error")
			return nil, err
		}
	}
	return GetUserProfile(conn, &common.Request{Id: in.Id, Type: in.Type})
}


func GetUserProfile(conn *connectiors.DBConnection, in *common.Request) (*profile.ProfileInfo, error) {
	//Generating the id
	id := fmt.Sprintf("%s::%s", in.Type, in.Id)

	//Travel Data from couchbase will be stored in data variable
	var data profile.ProfileInfo
	_, err := conn.Get(id, &data)
	if err != nil {
		errorMessage(err, "Error getting data")
		return nil, err
	}
	return &data, nil
}

func errorMessage(err error, context string) error {
	fmt.Println(context, err)
	return err
}
