package helper

import (
	"gitlab.com/restra-core/buffers/common"
	"gitlab.com/restra-core/buffers/place"
	"time"
)

func GetTravelPost() place.TravelPostParameter {
	addr := &common.Address{
		Type: "address",
		Division: &common.CodeSystem{
			Code:    100,
			Display: "Dhaka",
		},
		District: &common.CodeSystem{
			Code:    10,
			Display: "Dhaka",
		},
		Area: &common.CodeSystem{
			Code:    1,
			Display: "Banani",
		},
		Zip:    1212,
		Street: "Banani model town",
	}

	medium := &place.Medium{
		Way:   "string",
		Route: "string",
	}

	stay := &place.Stay{
		Additional: "Additional string",
		Place: &place.Place{
			Code:    112,
			Display: "chilekothar shepai",
		},
	}

	places := &common.CodeSystem{
		Code:    123,
		Display: "Bad",
	}

	post := place.TravelPost{
		Id:  "123",
		Uid: "432",
		Title: &common.Multilingual{
			En: "wow bd",
		},
		ProbableCost: &place.ProbableCost{
			Day:  1,
			Cost: 4500.00,
		},
		Description: "So amazing",
		Tags:        []string{"4321"},
		Visit: &place.VisitDuration{
			Start: time.Now().String(),
			End:   time.Now().String(),
		},
		Address: []*common.Address{
			addr,
		},
		Medium: []*place.Medium{medium},
		Stay:   []*place.Stay{stay},
		Places: []*common.CodeSystem{places},
		Type:   "travel",
	}

	var travelPost place.TravelPostParameter
	travelPost.Body = &post

	return travelPost
}
