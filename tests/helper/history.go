package helper

import "gitlab.com/post_activity/models"

func GenerateSampleHistory() *models.HistoryRequest {

	history := models.HistoryRequest{
		Id:   "0001",
		Type: "history",
		History: models.History{
			Published: "2011-02-10T15:04:55Z",
			Actor: models.Actor{
				ObjectType: "person",
				Id:         "profile::4321",
				Avatar: models.Image{
					Url: "www.someimage.com",
				},
				DisplayName: "Asif Arko",
			},
			Verb: "post",
			Object: models.Object{
				Id:    "travel::1234",
				Title: "some title",
			},
			Target: models.Target{
				ObjectType:  "person",
				Id:          "profile::4321",
				DisplayName: "Jonathan Mask",
			},
		},
	}
	return &history
}
