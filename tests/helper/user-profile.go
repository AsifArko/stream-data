package helper

import (
	"gitlab.com/restra-core/buffers/common"
	"gitlab.com/restra-core/buffers/profile"
)

func GetUserProfile() profile.ProfileInfo {

	prof := profile.ProfileInfo{
		Type: "profile",
		Id:   "7890",
		Name: &profile.Name{
			FirstName: "Sajjad",
			LastName:  "Fucker",
		},
		Gender: "Male",
		Address: &common.Address{
			Type: "address",
			Division: &common.CodeSystem{
				Code:    100,
				Display: "Dhaka",
			},
			District: &common.CodeSystem{
				Code:    10,
				Display: "Dhaka",
			},
			Area: &common.CodeSystem{
				Code:    1,
				Display: "Banani",
			},
			Zip:    1212,
			Street: "Banani model town",
		},
		Hobbies:    []string{"books , guitar"},
		Dob:        "03-03-1993",
		Registered: "registered",
		Picture: &profile.Picture{
			Profile: "profile picture",
			Cover:   "Cover picture",
		},
	}
	return prof
}
