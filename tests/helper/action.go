package helper

import "gitlab.com/post_activity/models"

func GenerateSampleAction() *models.ActionRequest {
	action := models.Action{
		Actor:  "profile::4567",
		Verb:   "Like",
		Target: "Friends",
		Object: "travel::1234",
		Date:   "2018-12-08 14:13:16.755867212 +0600 BDT m=+0.070928749",
	}
	return &models.ActionRequest{Id: "action::001", Action: action, Type: "action"}
}
